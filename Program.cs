﻿using System;
using System.Globalization;
using System.Collections.Generic;
using StackExchange.Redis;
using System.Drawing;

namespace RedisApp
{
   
    public class RedisCRUD
    {
        private static Lazy<ConnectionMultiplexer> lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
        {
            ConnectionMultiplexer connectionMultiplexer = ConnectionMultiplexer.Connect("127.0.0.1:6379");
            connectionMultiplexer.PreserveAsyncOrder = false;
            return connectionMultiplexer;
        });

        public static ConnectionMultiplexer Connection
        {
            get
            {
                return lazyConnection.Value;
            }
        }
        public void AddPoint(RedisKey key, double longtitude, double latitude, RedisValue member)
        {
            var PointToAdds = new Maps(key, longtitude, latitude, member);
            IDatabase db = Connection.GetDatabase();
            db.GeoAdd(PointToAdds.Key, PointToAdds.Longitude, PointToAdds.Latitude, PointToAdds.Member);
            Console.WriteLine("Point {0} has been added to DB", PointToAdds.Member);
        }

        public void ChceckNearestPoint(RedisKey key, double longtitude, double latitude, double radius)
        {
            IDatabase db = Connection.GetDatabase();
            //var result = db.GeoRadius(key, member, radius, GeoUnit.Kilometers, 2, Order.Ascending, GeoRadiusOptions.WithCoordinates);
            var result = db.GeoRadius(key, longtitude, latitude, radius, GeoUnit.Kilometers, 1, Order.Ascending, GeoRadiusOptions.WithCoordinates);
            
            foreach(var item in result)
            {
                Console.WriteLine(item);
            }
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            RedisCRUD redis = new RedisCRUD(); 
            redis.AddPoint("Seul", 126.988195, 37.551402, "N Seoul Tower");
            redis.AddPoint("Seul", 126.977032, 37.579752, "Gyeongbokgung");
            redis.AddPoint("Seul", 126.990933, 37.579122, "Gwonnong - dong");
            Console.WriteLine("#############################################");

            Console.WriteLine("Could you please share your position?");

            Console.WriteLine("Share longtitude:");
            double longtitude = Convert.ToDouble(Console.ReadLine(), CultureInfo.InvariantCulture);

            Console.WriteLine("Share latitude:");
            double latitude = Convert.ToDouble(Console.ReadLine(), CultureInfo.InvariantCulture);

            Console.WriteLine("Your nearest point is: ");
            redis.ChceckNearestPoint("Seul", longtitude, latitude, 500);
        }
    }
}
