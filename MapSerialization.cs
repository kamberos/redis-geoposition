﻿using System;
using System.Collections.Generic;
using System.Text;
using StackExchange.Redis;


namespace RedisApp
{
    public class Maps
    {
        public RedisKey Key { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }

        public RedisValue Member { get; set; }

        public Maps(RedisKey _key, double _longtitude, double _latitude, RedisValue _memeber)
        {
            Key = _key;
            Longitude = _longtitude;
            Latitude = _latitude;
            Member = _memeber;
        }

    }
}
